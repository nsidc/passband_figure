#!/bin/bash

# Make several plots with successively added layers.

# This was for a CHARIS figure.

out_root='charis_spectra_'

# Only color background
./mk_snow_passband_fig.sh -o ${out_root}_layer01.ps -c

# Add reflectance spectra for materials
./mk_snow_passband_fig.sh -o ${out_root}_layer02.ps -c -r

# Add pass bands for sensors
./mk_snow_passband_fig.sh -o ${out_root}_layer03.ps -c -r -p
