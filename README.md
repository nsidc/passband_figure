# README #

This is a small collection of scripts that use the [GMT software tools](http://gmt.soest.hawaii.edu/) to create a figure showing pass bands for several satellite instruments and reflectance spectra for a few materials.  The figure was used, for example, in

Remote Sensing of the Cryosphere, Marco Tedesco ed., Chapter 7, Remote Sensing of Glaciers

### Example Output ###

![Example figure](passbands_snow_vnir_sm.png?raw=true)

### Dependencies ###

* bash
* Generic Mapping Tools (GMT)

### Who do I talk to? ###

* Bruce Raup (braup@nsidc.org)
