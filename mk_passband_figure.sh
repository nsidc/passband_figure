#!/bin/bash
# $Id: mk_passband_figure,v 1.8 2001-11-07 10:20:11-07 braup Exp braup $
# Reads a file of instrument passbands and uses GMT to create a plot of the
# passbands by instrument.

version='$Revision: 1.8 $'
version=`echo $version | sed 's/^\$Revision: *//' \
         | sed 's/ *\$$//'`

progver="`basename $0` version $version (B. Raup)"

GMT='/usr/bin/gmt'

# Set defaults
verbose='OFF'
passbands='OFF'
color='OFF'
refl='OFF'
inst_dir='Instrument_passbands'
infile_suffix='dat'
out_vnir=passbands_snow_vnir.ps
box_fill=240
box_pen="0.05p,0/0/0"
bg_color="255/255/255"
plot_height=15  # centimeters
y_max=100
plot_width=20   # centimeters

# passband row height and gap
bar_width=3   # centimeters
rowgap=0.6 # cm

label_x=2.85
t_size=8       # text size, in points, for box labels
spect_annot_pen='8,Helvetica,black'
spect_annot_bgcolor='salmon1'
name_text_color='black'
name_text_font='Helvetica'
name_text_size=7
bandnum_color=0
min_lambda_vnir=0.3  # micrometers
max_lambda_vnir=3.2 # micrometers
#max_lambda_vnir=12  # micrometers

num_inst=$(ls -1 ${inst_dir}/*.${infile_suffix} | wc -l | sed 's/^  *//')
#num_inst=20

ref_spectra_dir='Spectra/'

region="-R$min_lambda_vnir/$max_lambda_vnir/0/$y_max"


# Define functions

function do_box {
  x1=$1;
  x2=$2
  y=$3;
  w=$4
  band_num=$5
  out=$6
  minl=$7
  maxl=$8
  y2=`echo "scale=20; $y + $w" | bc`
# echo "do_box:  x1=$x1, x2=$x2, y=$y, y2=$y2, w=$w, out=$out"
$GMT psxy $region -JX -O -K -G$box_fill -W$box_pen -V <<EOF >> $out
$x1 $y
$x2 $y
$x2 $y2
$x1 $y2
EOF
  avg_x=`echo "scale=20; ($x1 + $x2) / 2.0" | bc`
  avg_y=`echo "scale=20; ($y + $y2) / 2.0" | bc`
  echo "$avg_x $avg_y $band_num" |
      $GMT pstext $region -JX $verbose -F+f$name_text_size,$name_text_font,$name_text_color -O -K >> $out
}

usage="This is $progver

Usage:  $0 [-h]
or
        $0 [-b] [-c] [-r] [-o output_file_vnir] [-V]
where
  -c    plot Color background
  -h    prints this help message and exits
  -o    specify output filename [$out_vnir]
  -p    plot sensor Pass bands
  -r    plot Reflectance spectra for various materials
  -V    specifies verbose mode
"

while getopts ":Vpo:rch" opt ; do
  case "$opt" in
    p)
        passbands='ON'
        ;;
    c)
        color='ON'
        ;;
    r)
        refl='ON'
        ;;
    V)
        verbose='-V'
        ;;
    h)
        echo "$usage"
        exit 0
        ;;
    o)
        out_vnir=$OPTARG
        ;;
    \?)
        echo "$OPTARG is not a valid option.  Sorry."
        echo "$usage"
        exit 1
        ;;
    :)
        echo "$OPTARG requires an argument.  Sorry."
        echo "$usage"
        exit 1
        ;;
  esac
done
shift `expr $OPTIND - 1`

if [ ! $# -eq 0 ] ; then
  echo "$usage"
  exit 1
fi

if [ "$verbose" = "OFF" ] ; then
  verbose=''
fi

if [ "$verbose" = "-V" ] ; then
  echo "This is $progver" >&2
  echo "Writing output to $out_vnir and $out_tir" >&2
fi

# Need to fit all the instrument passband rows into the plot height

#y_inc=`echo "scale=10; ($y_max-2*$bar_width)/($num_inst-1)" | bc`
#echo "y_inc = $y_inc"
#y=`echo "scale=10; $y_max-2*$bar_width" | bc`

y_inc=`echo "scale=10; $bar_width + $rowgap" | bc`
y=`echo "scale=10; $num_inst*($bar_width + $rowgap)" | bc`
echo "y_inc = $y_inc"

$GMT gmtset PS_MEDIA a4 FONT_TITLE 24 FONT_ANNOT_PRIMARY 13p,Helvetica,black FONT_LABEL 18 MAP_GRID_PEN_PRIMARY 0.1p,220/220/220
# MAP_GRID_PEN_PRIMARY 0.1,255/220/180t2_4:8p

if [ "$verbose" = "-V" ] ; then
  echo "Setting up plots" >&2
fi

# Dummy start
$GMT psxy /dev/null $region -JX${plot_width}c/${plot_height}c -W1p,black -K > $out_vnir

$GMT psbasemap $region  -U'Bruce H. Raup, NSIDC' \
  -JX${plot_width}c/${plot_height}c -K -O -G$bg_color $verbose \
  -Ba0.2:"Wavelength (@~m@~m)":/a20:"Reflectance (%)"::."Instrument pass bands and spectral reflectances":Wen \
  >> $out_vnir

# Add rainbow colorbar
if [ "$color" = 'ON' ] ; then
    #makecpt -T0.38/0.75/0.05 -Z -Crainbow > rainbow_start.cpt
    cat <<EOF > rainbow_start.cpt
#	cpt file created by: makecpt -T0.38/0.75/0.05 -Z -Crainbow
#	cpt file created by: makecpt -T0.4/0.70/0.05 -Z -Crainbow
#COLOR_MODEL = +HSV
#
0.30	300	0.1	1	0.38	300	0.5	1
0.38	300	0.5	1	0.40	300	1	1
0.4	300	1	1	0.45	250	1	1
0.45	250	1	1	0.5	200	1	1
0.5	200	1	1	0.55	150	1	1
0.55	150	1	1	0.6	100	1	1
0.6	100	1	1	0.62	50	1	1
0.62	50	1	1	0.68	0	1	1
0.68    0       1   1  0.85    0  0.3   1
0.85    0      0.3   1   3.2    0  0.0   1
B	0	0	0
F	0	0	1
N	0	0	0.498039
EOF
    #######################3
    #	cpt file created by: makecpt -T0.4/0.70/0.05 -Z -Crainbow
    #COLOR_MODEL = +HSV
    #
    #0.4	300	1	1	0.45	250	1	1
    #0.45	250	1	1	0.5	200	1	1
    #0.5	200	1	1	0.55	150	1	1
    #0.55	150	1	1	0.6	100	1	1
    #0.6	100	1	1	0.65	50	1	1
    #0.65	50	1	1	0.7	0	1	1
    #B	0	0	0
    #F	0	0	1
    #N	0	0	0.498039
    #######################3
    #0.43	257.143	1	1	0.48	214.286	1	1
    #0.48	214.286	1	1	0.53	171.429	1	1
    #0.53	171.429	1	1	0.58	128.571	1	1
    #0.58	128.571	1	1	0.63	85.7143	1	1
    #0.63	85.7143	1	1	0.68	42.8571	1	1
    #0.68	42.8571	1	1	0.73	0	1	1
    
    scale_pos_x=$(echo "scale=20; $plot_width / 2" | bc)
    scale_pos_y=$plot_height
    $GMT psscale -Crainbow_start.cpt -D$scale_pos_x/$scale_pos_y/$plot_width/${plot_height}h \
      -Ba0.2:"Wavelength (@~m@~m)":S  -K -O -V >> $out_vnir
fi

if [ "$passbands" = "ON" ] ; then
    # Plot pass bands
    
    #for infile in $inst_dir/ASTER.dat $inst_dir/TM.dat ; do
    
    # Full figure:
    #for infile in ASTER.dat EO-1_ALI.dat EO-1_HYPERION.dat ETM\+.dat ETM_PAN.dat Landsat8.dat Landsat8_Pan.dat MODIS.dat MSS.dat QUICKBIRD_MULTI.dat SPOT.dat SPOT_PAN.dat TM.dat ; do
    for infile in $inst_dir/*.${infile_suffix} ; do
      name=`basename $infile $infile_suffix | sed 's/\.$//' | sed -e 's/_/, /g'`
      if [ "$verbose" = "-V" ] ; then
        echo "name = $name" >&2
      fi
      grep -v '^#' $infile | while read band_num x1 x2 ; do
        do_box $x1 $x2 $y $bar_width $band_num $out_vnir $min_lambda_vnir $max_lambda_vnir
      done
    # label_y=`echo "scale=10; $y + $bar_width + 0.1" | bc` # That's 0.1 cm
      label_y=`echo "scale=10; $y + $bar_width/2" | bc`
      echo "$label_x $label_y $name" | $GMT pstext $region -JX \
             -F+f10,Helvetica,black $verbose -D0.1c/0 -O -K >> $out_vnir
      y=`echo "scale=10; $y - $y_inc" | bc`
    done
fi
    
    # Label text rectangle clearance
    clearance='10%'
    
if [ "$refl" = "ON" ] ; then
    # Plot snow spectra
    filter_width=0.1  # micrometers
    # Fine snow
    datfile="$ref_spectra_dir/fine.snw"
    $GMT filter1d $datfile -Fg$filter_width -V  |
         awk '($1 <= 2.5) {print}' |
         $GMT psxy                                          \
         $region                                            \
         -JX${plot_width}c/${plot_height}c                  \
         -W1p,cyan                                          \
         -K -O $verbose >> $out_vnir
    echo "0.92 93 Fine-grain snow" |
        $GMT pstext $region -JX -C$clearance -F+f$spect_annot_pen+jLM -G$spect_annot_bgcolor -Dj0.5c/0.2c+v1p,cyan -K -O -V >> $out_vnir
    
    # Coarse snow
    datfile="$ref_spectra_dir/coarse.snw"
    $GMT filter1d $datfile -Fg$filter_width -V  |
         awk '($1 <= 2.5) {print}' |
         $GMT psxy                                               \
         $region                                            \
         -JX${plot_width}c/${plot_height}c                  \
         -W1p,blue                                          \
         -K -O $verbose >> $out_vnir
    echo "0.8 85 Coarse-grain snow" | $GMT pstext $region -JX -C$clearance -F+f$spect_annot_pen+jRT -G$spect_annot_bgcolor -Dj0.2c/0.2c+v1p,blue -K -O -V >> $out_vnir
    
    # Plot glacier ice spectrum
    datfile="$ref_spectra_dir/glacier_ice_spectrum.dat"
    $GMT psxy $datfile                                           \
         $region            \
         -JX${plot_width}c/${plot_height}c                  \
         -W1p,brown                                         \
         -K -O $verbose >> $out_vnir
    echo "0.5 60 Glacier ice" | $GMT pstext $region -JX -C$clearance -F+f$spect_annot_pen+jCB -G$spect_annot_bgcolor -Dj-0.5c/0.5c+v1p,brown -K -O -V >> $out_vnir
    
    # Plot basalt spectrum
    datfile="$ref_spectra_dir/basalt.h1"
    $GMT psxy $datfile                                           \
         $region            \
         -JX${plot_width}c/${plot_height}c                  \
         -W1p,darkbrown                                         \
         -K -O $verbose >> $out_vnir
    echo "1.3 5 Basalt" | $GMT pstext $region -JX -C$clearance -F+f$spect_annot_pen+jCB -G$spect_annot_bgcolor -K -O -V >> $out_vnir
    
    # Plot vegetation (deciduous) spectrum
    datfile="$ref_spectra_dir/decidous.dat"
    $GMT psxy $datfile                                           \
         $region            \
         -JX${plot_width}c/${plot_height}c                  \
         -W1p,green                                         \
         -K -O $verbose >> $out_vnir
    echo "1.82 23 Deciduous Vegetation" | $GMT pstext $region -JX -F+f$spect_annot_pen+jLB -G$spect_annot_bgcolor -C$clearance -Dj0.2c/0.3c+v1p,green -K -O -V >> $out_vnir
fi
    
    # Dummy calls to finish the plots
$GMT psxy /dev/null $region -JX -Sp -O >> $out_vnir

# Convert to PNG
namebase=$(basename $out_vnir .ps)
convert -rotate 90 -density 120 -trim $out_vnir $namebase.png
convert -rotate 90 -density 80 -trim $out_vnir ${namebase}_sm.png
