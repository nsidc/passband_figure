#!/usr/bin/env python3

# This is to create the wavelength file for the proposed bands in Landsat 9 SLI
# (Sustainable Land Imaging)

center_wavelength = [
    410,
    443,
    490,
    560,
    620,
    665,
    705,
    740,
    783,
    842,
    865,
    945,
    1035,
    1375,
    1610,
    2040,
    2100,
    2210,
    3980,
    8300,
    8650,
    9100,
    10800,
    12000,
]

bandwidth = [
    20,
    20,
    65,
    35,
    20,
    30,
    15,
    15,
    20,
    115,
    20,
    20,
    20,
    30,
    90,
    30,
    40,
    40,
    300,
    350,
    350,
    350,
    1000,
    1000,
]

names = [
    '1a',
    '1',
    '2',
    '3',
    '4a',
    '4',
    '5',
    '6',
    '7',
    '8',
    '8a',
    '9',
    '8b',
    '10',
    '11',
    '12a',
    '12b',
    '12c',
    '13',
    '14a',
    '14',
    '14b',
    '15',
    '16',
]

print("# Name, left_edge, right_edge")
for n, L, w in zip(names, center_wavelength, bandwidth):
    low = L - w/2.0
    high = L + w/2.0
    print(n, low, high)


